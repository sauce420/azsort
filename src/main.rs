use std::io::{self, BufRead};
use std::path::Path;
use std::fs::{write, File}; 
#[macro_use]
extern crate text_io;

fn main() {
    println!("This program sorts any given array in rust alphanumerically");
    println!("Read from a file or input data manually? [f/i]");
    let file_or_input: String = read!();
    match file_or_input.as_str() {
        "f" => {
            println!("Enter full path to file");
            let filepath: String = read!();
            load(filepath);
        }
        "i" => {
            input();
        }
        _ => {
            println!("Please type either f or i.");
            main();
        }
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn load(filepath: String) {
    let mut strvec: Vec<String> = Vec::new();
    if let Ok(lines) = read_lines(filepath) {
        for line in lines {
                strvec.push(line.unwrap());
        }
    }
    save(strvec);
}

fn input() {
    let mut strvec: Vec<String> = Vec::new(); 
    let mut input: String; 
    println!("Type any word/number then press enter to add it to the array.\n Type STOP to end the input.");
    loop{ //loop, appending user input to strvec every time enter is pressed. The keyword to stop is STOP.
        input = read!();
        if input == "STOP" {
            break
        }
        strvec.push(input);
    }
    print!("You've typed STOP. ");
    save(strvec);
}

fn save(mut strvec: Vec<String>) {
    println!("Showing sorted results now.");
    strvec.sort();
    for i in &strvec {
        println!("{}", i);
    }
    println!("Save sorted results to file? Type y or n");
    let to_save: String = read!();
    match to_save.as_str() {
        "y" => {
            println!("Enter filename");
            let filename: String = read!();
            write(filename, strvec.join("\n")).expect("Failed to write to file"); 
            //separate out the vector's members with a newline 
            exit_or_rerun();
        }
        "n" => {
            exit_or_rerun();
        }
        _ => {
            println!("Please type y or n");
            save(strvec);
        }
    }
}

fn exit_or_rerun() {
    println!("Run again? Type y or n");
    let to_rerun: String = read!();
    match to_rerun.as_str() {
        "y" => {
            main();
        }
        "n" => {
            std::process::exit(0);
        }
        _ => {
            println!("Please enter either y or n");
            exit_or_rerun();
        }
    }
}

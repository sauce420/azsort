# Azsort
A program to sort user input alphanumerically, either through an input file, or through stdin. <br>
<br>
## Functionality<br>
>Reading from files<br>
>Writing to files<br>
>The Rust standard library's implementation of a sorting algorithm.(the `.sort()` method)<br>
>Reading from user input<br>
## Licence<br>
The AGPLv3 License (AGPLv3)

Copyright (c) 2022 Jeremy Petch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
